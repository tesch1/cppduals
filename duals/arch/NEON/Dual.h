// This file is part of Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2010 Gael Guennebaud <gael.guennebaud@inria.fr>
// Copyright (C) 2010 Konstantinos Margaritis <markos@freevec.org>
// Copyright (C) 2020 Michael Tesch <tesch1@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_DUAL_NEON_H
#define EIGEN_DUAL_NEON_H

namespace Eigen {

namespace internal {

#ifndef EIGEN_COMPLEX_NEON_H
inline uint32x4_t p4ui_CONJ_XOR() {
// See bug 1325, clang fails to call vld1q_u64.
#if EIGEN_COMP_CLANG
  uint32x4_t ret = { 0x00000000, 0x80000000, 0x00000000, 0x80000000 };
  return ret;
#else
  static const uint32_t conj_XOR_DATA[] = { 0x00000000, 0x80000000, 0x00000000, 0x80000000 };
  return vld1q_u32( conj_XOR_DATA );
#endif
}

inline uint32x2_t p2ui_CONJ_XOR() {
  static const uint32_t conj_XOR_DATA[] = { 0x00000000, 0x80000000 };
  return vld1_u32( conj_XOR_DATA );
}
#endif // EIGEN_COMPLEX_NEON_H

//---------- float ----------
struct Packet2df
{
  EIGEN_STRONG_INLINE Packet2df() {}
  EIGEN_STRONG_INLINE explicit Packet2df(const Packet4f& a) : v(a) {}
  Packet4f  v;
};

template<> struct packet_traits<duals::dual<float> >  : default_packet_traits
{
  typedef Packet2df type;
  typedef Packet2df half;
  enum {
    Vectorizable = 1,
    AlignedOnScalar = 1,
    size = 2,
    HasHalfPacket = 0,

    HasAdd    = 1,
    HasSub    = 1,
    HasMul    = 1,
    HasDiv    = 1,
    HasNegate = 1,
    HasAbs    = 0,
    HasAbs2   = 0,
    HasMin    = 0,
    HasMax    = 0,
    HasSetLinear = 0
  };
};

template<> struct unpacket_traits<Packet2df> { typedef duals::dual<float> type; enum {size=2, alignment=Aligned16}; typedef Packet2df half; };

template<> EIGEN_STRONG_INLINE Packet2df pset1<Packet2df>(const duals::dual<float>&  from)
{
  float32x2_t r64;
  r64 = vld1_f32((const float *)&from);

  return Packet2df(vcombine_f32(r64, r64));
}

template<> EIGEN_STRONG_INLINE Packet2df padd<Packet2df>(const Packet2df& a, const Packet2df& b) { return Packet2df(padd<Packet4f>(a.v,b.v)); }
template<> EIGEN_STRONG_INLINE Packet2df psub<Packet2df>(const Packet2df& a, const Packet2df& b) { return Packet2df(psub<Packet4f>(a.v,b.v)); }
template<> EIGEN_STRONG_INLINE Packet2df pnegate(const Packet2df& a) { return Packet2df(pnegate<Packet4f>(a.v)); }
template<> EIGEN_STRONG_INLINE Packet2df pdconj(const Packet2df& a)
{
  Packet4ui b = vreinterpretq_u32_f32(a.v);
  return Packet2df(vreinterpretq_f32_u32(veorq_u32(b, p4ui_CONJ_XOR())));
}
template<> EIGEN_STRONG_INLINE Packet2df pconj(const Packet2df& a) { return a; }

template<> EIGEN_STRONG_INLINE Packet2df pmul<Packet2df>(const Packet2df& a, const Packet2df& b)
{
  Packet4f v1, v2;
  uint32x4_t mask = {0x00000000, 0xffffffff, 0x00000000, 0xffffffff};
  v1 = vcombine_f32(vdup_lane_f32(vget_low_f32(b.v), 0), vdup_lane_f32(vget_high_f32(b.v), 0));
  v2 = vcombine_f32(vdup_lane_f32(vget_low_f32(a.v), 0), vdup_lane_f32(vget_high_f32(a.v), 0));
  return Packet2df(vaddq_f32(vreinterpretq_f32_u32(vandq_u32(mask,
                                                             vreinterpretq_u32_f32(vmulq_f32(a.v, v1)))),
                             vmulq_f32(b.v, v2)));
}

template<> EIGEN_STRONG_INLINE Packet2df pand   <Packet2df>(const Packet2df& a, const Packet2df& b)
{
  return Packet2df(vreinterpretq_f32_u32(vandq_u32(vreinterpretq_u32_f32(a.v),vreinterpretq_u32_f32(b.v))));
}
template<> EIGEN_STRONG_INLINE Packet2df por    <Packet2df>(const Packet2df& a, const Packet2df& b)
{
  return Packet2df(vreinterpretq_f32_u32(vorrq_u32(vreinterpretq_u32_f32(a.v),vreinterpretq_u32_f32(b.v))));
}
template<> EIGEN_STRONG_INLINE Packet2df pxor   <Packet2df>(const Packet2df& a, const Packet2df& b)
{
  return Packet2df(vreinterpretq_f32_u32(veorq_u32(vreinterpretq_u32_f32(a.v),vreinterpretq_u32_f32(b.v))));
}
template<> EIGEN_STRONG_INLINE Packet2df pandnot<Packet2df>(const Packet2df& a, const Packet2df& b)
{
  return Packet2df(vreinterpretq_f32_u32(vbicq_u32(vreinterpretq_u32_f32(a.v),vreinterpretq_u32_f32(b.v))));
}

template<> EIGEN_STRONG_INLINE Packet2df pload<Packet2df>(const duals::dual<float>* from) {
  EIGEN_DEBUG_ALIGNED_LOAD return Packet2df(pload<Packet4f>((const float*)from)); }
template<> EIGEN_STRONG_INLINE Packet2df ploadu<Packet2df>(const duals::dual<float>* from) {
  EIGEN_DEBUG_UNALIGNED_LOAD return Packet2df(ploadu<Packet4f>((const float*)from)); }

template<> EIGEN_STRONG_INLINE Packet2df ploaddup<Packet2df>(const duals::dual<float>* from) { return pset1<Packet2df>(*from); }

template<> EIGEN_STRONG_INLINE void pstore <duals::dual<float> >(duals::dual<float> *   to, const Packet2df& from)
{ EIGEN_DEBUG_ALIGNED_STORE pstore((float*)to, from.v); }
template<> EIGEN_STRONG_INLINE void pstoreu<duals::dual<float> >(duals::dual<float> *   to, const Packet2df& from)
{ EIGEN_DEBUG_UNALIGNED_STORE pstoreu((float*)to, from.v); }

template<> EIGEN_DEVICE_FUNC inline Packet2df pgather<duals::dual<float>, Packet2df>(const duals::dual<float>* from, Index stride)
{
  Packet4f res = pset1<Packet4f>(0.f);
  res = vsetq_lane_f32(duals::rpart(from[0*stride]), res, 0);
  res = vsetq_lane_f32(duals::dpart(from[0*stride]), res, 1);
  res = vsetq_lane_f32(duals::rpart(from[1*stride]), res, 2);
  res = vsetq_lane_f32(duals::dpart(from[1*stride]), res, 3);
  return Packet2df(res);
}

template<> EIGEN_DEVICE_FUNC inline void pscatter<duals::dual<float>, Packet2df>(duals::dual<float>* to, const Packet2df& from, Index stride)
{
  to[stride*0] = duals::dual<float>(vgetq_lane_f32(from.v, 0), vgetq_lane_f32(from.v, 1));
  to[stride*1] = duals::dual<float>(vgetq_lane_f32(from.v, 2), vgetq_lane_f32(from.v, 3));
}

template<> EIGEN_STRONG_INLINE void prefetch<duals::dual<float> >(const duals::dual<float> *   addr) { EIGEN_ARM_PREFETCH((const float *)addr); }

template<> EIGEN_STRONG_INLINE duals::dual<float>  pfirst<Packet2df>(const Packet2df& a)
{
  duals::dual<float> EIGEN_ALIGN16 x[2];
  vst1q_f32((float *)x, a.v);
  return x[0];
}

template<> EIGEN_STRONG_INLINE Packet2df preverse(const Packet2df& a)
{
  float32x2_t a_lo, a_hi;
  Packet4f a_r128;

  a_lo = vget_low_f32(a.v);
  a_hi = vget_high_f32(a.v);
  a_r128 = vcombine_f32(a_hi, a_lo);

  return Packet2df(a_r128);
}

#if 0
template<> EIGEN_STRONG_INLINE Packet2df pcplxflip<Packet2df>(const Packet2df& a)
{
  return Packet2df(vrev64q_f32(a.v));
}
#endif

template<> EIGEN_STRONG_INLINE duals::dual<float> predux<Packet2df>(const Packet2df& a)
{
  float32x2_t a1, a2;
  duals::dual<float> s;

  a1 = vget_low_f32(a.v);
  a2 = vget_high_f32(a.v);
  a2 = vadd_f32(a1, a2);
  vst1_f32((float *)&s, a2);

  return s;
}

template<> EIGEN_STRONG_INLINE Packet2df preduxp<Packet2df>(const Packet2df* vecs)
{
  Packet4f sum1, sum2, sum;

  // Add the first two 64-bit float32x2_t of vecs[0]
  sum1 = vcombine_f32(vget_low_f32(vecs[0].v), vget_low_f32(vecs[1].v));
  sum2 = vcombine_f32(vget_high_f32(vecs[0].v), vget_high_f32(vecs[1].v));
  sum = vaddq_f32(sum1, sum2);

  return Packet2df(sum);
}

template<> EIGEN_STRONG_INLINE duals::dual<float> predux_mul<Packet2df>(const Packet2df& a)
{
  return pfirst(pmul(a, Packet2df(vcombine_f32(vget_high_f32(a.v), vget_high_f32(a.v)))));
}

template<int Offset>
struct palign_impl<Offset,Packet2df>
{
  EIGEN_STRONG_INLINE static void run(Packet2df& first, const Packet2df& second)
  {
    if (Offset==1)
    {
      first.v = vextq_f32(first.v, second.v, 2);
    }
  }
};

template<> struct conj_helper<Packet2df, Packet2df, false,true>
{
  EIGEN_STRONG_INLINE Packet2df pmadd(const Packet2df& x, const Packet2df& y, const Packet2df& c) const
  { return padd(pmul(x,y),c); }

  EIGEN_STRONG_INLINE Packet2df pmul(const Packet2df& a, const Packet2df& b) const
  {
    return internal::pmul(a, pconj(b));
  }
};

template<> struct conj_helper<Packet2df, Packet2df, true,false>
{
  EIGEN_STRONG_INLINE Packet2df pmadd(const Packet2df& x, const Packet2df& y, const Packet2df& c) const
  { return padd(pmul(x,y),c); }

  EIGEN_STRONG_INLINE Packet2df pmul(const Packet2df& a, const Packet2df& b) const
  {
    return internal::pmul(pconj(a), b);
  }
};

template<> struct conj_helper<Packet2df, Packet2df, true,true>
{
  EIGEN_STRONG_INLINE Packet2df pmadd(const Packet2df& x, const Packet2df& y, const Packet2df& c) const
  { return padd(pmul(x,y),c); }

  EIGEN_STRONG_INLINE Packet2df pmul(const Packet2df& a, const Packet2df& b) const
  {
    return pconj(internal::pmul(a, b));
  }
};

EIGEN_MAKE_CONJ_HELPER_CPLX_REAL(Packet2df,Packet4f)

template<> EIGEN_STRONG_INLINE Packet2df pdiv<Packet2df>(const Packet2df& a, const Packet2df& b)
{
  uint32x4_t mask = {0xffffffff, 0x00000000, 0xffffffff, 0x00000000};
  float32x4_t c = vcombine_f32(vdup_lane_f32(vget_low_f32(b.v), 0), vdup_lane_f32(vget_high_f32(b.v), 0));
  float32x4_t d = vcombine_f32(vdup_lane_f32(vget_low_f32(a.v), 0), vdup_lane_f32(vget_high_f32(a.v), 0));
  return Packet2df(pdiv<Packet4f>
                   (vaddq_f32(vreinterpretq_f32_u32
                              (vandq_u32(mask,
                                         vreinterpretq_u32_f32(a.v))),
                              vreinterpretq_f32_u32
                              (vandq_u32(vmvnq_u32(mask),
                                         vreinterpretq_u32_f32(vsubq_f32(vmulq_f32(a.v, c),
                                                                         vmulq_f32(b.v, d)))))),
                    vaddq_f32(vreinterpretq_f32_u32(vandq_u32(mask,
                                                              vreinterpretq_u32_f32(b.v))),
                              vreinterpretq_f32_u32(vandq_u32(vmvnq_u32(mask),
                                                              vreinterpretq_u32_f32(vmulq_f32(c, c)))))));
}

EIGEN_DEVICE_FUNC inline void
ptranspose(PacketBlock<Packet2df,2>& kernel) {
  Packet4f tmp = vcombine_f32(vget_high_f32(kernel.packet[0].v), vget_high_f32(kernel.packet[1].v));
  kernel.packet[0].v = vcombine_f32(vget_low_f32(kernel.packet[0].v), vget_low_f32(kernel.packet[1].v));
  kernel.packet[1].v = tmp;
}

//---------- double ----------
#if EIGEN_ARCH_ARM64 && !EIGEN_APPLE_DOUBLE_NEON_BUG

// See bug 1325, clang fails to call vld1q_u64.
#ifndef EIGEN_COMPLEX_NEON_H
#if EIGEN_COMP_CLANG
  static uint64x2_t p2ul_CONJ_XOR = {0x0, 0x8000000000000000};
#else
  const uint64_t  p2ul_conj_XOR_DATA[] = { 0x0, 0x8000000000000000 };
  static uint64x2_t p2ul_CONJ_XOR = vld1q_u64( p2ul_conj_XOR_DATA );
#endif
#endif // EIGEN_COMPLEX_NEON_H

struct Packet1dd
{
  EIGEN_STRONG_INLINE Packet1dd() {}
  EIGEN_STRONG_INLINE explicit Packet1dd(const Packet2d& a) : v(a) {}
  Packet2d v;
};

template<> struct packet_traits<duals::dual<double> >  : default_packet_traits
{
  typedef Packet1dd type;
  typedef Packet1dd half;
  enum {
    Vectorizable = 1,
    AlignedOnScalar = 0,
    size = 1,
    HasHalfPacket = 0,

    HasAdd    = 1,
    HasSub    = 1,
    HasMul    = 1,
    HasDiv    = 1,
    HasNegate = 1,
    HasAbs    = 0,
    HasAbs2   = 0,
    HasMin    = 0,
    HasMax    = 0,
    HasSetLinear = 0
  };
};

template<> struct unpacket_traits<Packet1dd> { typedef duals::dual<double> type; enum {size=1, alignment=Aligned16}; typedef Packet1dd half; };

template<> EIGEN_STRONG_INLINE Packet1dd pload<Packet1dd>(const duals::dual<double>* from) { EIGEN_DEBUG_ALIGNED_LOAD return Packet1dd(pload<Packet2d>((const double*)from)); }
template<> EIGEN_STRONG_INLINE Packet1dd ploadu<Packet1dd>(const duals::dual<double>* from) { EIGEN_DEBUG_UNALIGNED_LOAD return Packet1dd(ploadu<Packet2d>((const double*)from)); }

template<> EIGEN_STRONG_INLINE Packet1dd pset1<Packet1dd>(const duals::dual<double>&  from)
{ /* here we really have to use unaligned loads :( */ return ploadu<Packet1dd>(&from); }

template<> EIGEN_STRONG_INLINE Packet1dd padd<Packet1dd>(const Packet1dd& a, const Packet1dd& b) { return Packet1dd(padd<Packet2d>(a.v,b.v)); }
template<> EIGEN_STRONG_INLINE Packet1dd psub<Packet1dd>(const Packet1dd& a, const Packet1dd& b) { return Packet1dd(psub<Packet2d>(a.v,b.v)); }
template<> EIGEN_STRONG_INLINE Packet1dd pnegate(const Packet1dd& a) { return Packet1dd(pnegate<Packet2d>(a.v)); }
template<> EIGEN_STRONG_INLINE Packet1dd pconj(const Packet1dd& a) { return a; }

template<> EIGEN_STRONG_INLINE Packet1dd pmul<Packet1dd>(const Packet1dd& a, const Packet1dd& b)
{
  const uint64x2_t mask = {0, 0xffffffffffffffff};
  return Packet1dd
    (vaddq_f64(vreinterpretq_f64_u64
               (vandq_u64(mask,
                          vreinterpretq_u64_f64(vmulq_f64(a.v,
                                                          vdupq_lane_f64(vget_low_f64(b.v), 0))))),
               vmulq_f64(vdupq_lane_f64(vget_low_f64(a.v), 0),
                         b.v)));

}

template<> EIGEN_STRONG_INLINE Packet1dd pand   <Packet1dd>(const Packet1dd& a, const Packet1dd& b)
{
  return Packet1dd(vreinterpretq_f64_u64(vandq_u64(vreinterpretq_u64_f64(a.v),vreinterpretq_u64_f64(b.v))));
}
template<> EIGEN_STRONG_INLINE Packet1dd por    <Packet1dd>(const Packet1dd& a, const Packet1dd& b)
{
  return Packet1dd(vreinterpretq_f64_u64(vorrq_u64(vreinterpretq_u64_f64(a.v),vreinterpretq_u64_f64(b.v))));
}
template<> EIGEN_STRONG_INLINE Packet1dd pxor   <Packet1dd>(const Packet1dd& a, const Packet1dd& b)
{
  return Packet1dd(vreinterpretq_f64_u64(veorq_u64(vreinterpretq_u64_f64(a.v),vreinterpretq_u64_f64(b.v))));
}
template<> EIGEN_STRONG_INLINE Packet1dd pandnot<Packet1dd>(const Packet1dd& a, const Packet1dd& b)
{
  return Packet1dd(vreinterpretq_f64_u64(vbicq_u64(vreinterpretq_u64_f64(a.v),vreinterpretq_u64_f64(b.v))));
}

template<> EIGEN_STRONG_INLINE Packet1dd ploaddup<Packet1dd>(const duals::dual<double>* from) { return pset1<Packet1dd>(*from); }

template<> EIGEN_STRONG_INLINE void pstore <duals::dual<double> >(duals::dual<double> *   to, const Packet1dd& from) { EIGEN_DEBUG_ALIGNED_STORE pstore((double*)to, from.v); }
template<> EIGEN_STRONG_INLINE void pstoreu<duals::dual<double> >(duals::dual<double> *   to, const Packet1dd& from) { EIGEN_DEBUG_UNALIGNED_STORE pstoreu((double*)to, from.v); }

template<> EIGEN_STRONG_INLINE void prefetch<duals::dual<double> >(const duals::dual<double> *   addr) { EIGEN_ARM_PREFETCH((const double *)addr); }

template<> EIGEN_DEVICE_FUNC inline Packet1dd pgather<duals::dual<double>, Packet1dd>(const duals::dual<double>* from, Index stride)
{
  Packet2d res = pset1<Packet2d>(0.0);
  res = vsetq_lane_f64(duals::rpart(from[0*stride]), res, 0);
  res = vsetq_lane_f64(duals::dpart(from[0*stride]), res, 1);
  return Packet1dd(res);
}

template<> EIGEN_DEVICE_FUNC inline void pscatter<duals::dual<double>, Packet1dd>(duals::dual<double>* to, const Packet1dd& from, Index stride)
{
  to[stride*0] = duals::dual<double>(vgetq_lane_f64(from.v, 0), vgetq_lane_f64(from.v, 1));
}


template<> EIGEN_STRONG_INLINE duals::dual<double>  pfirst<Packet1dd>(const Packet1dd& a)
{
  duals::dual<double> EIGEN_ALIGN16 res;
  pstore<duals::dual<double> >(&res, a);

  return res;
}

template<> EIGEN_STRONG_INLINE Packet1dd preverse(const Packet1dd& a) { return a; }

template<> EIGEN_STRONG_INLINE duals::dual<double> predux<Packet1dd>(const Packet1dd& a) { return pfirst(a); }

template<> EIGEN_STRONG_INLINE Packet1dd preduxp<Packet1dd>(const Packet1dd* vecs) { return vecs[0]; }

template<> EIGEN_STRONG_INLINE duals::dual<double> predux_mul<Packet1dd>(const Packet1dd& a) { return pfirst(a); }

template<int Offset>
struct palign_impl<Offset,Packet1dd>
{
  static EIGEN_STRONG_INLINE void run(Packet1dd& /*first*/, const Packet1dd& /*second*/)
  {
    // FIXME is it sure we never have to align a Packet1dd?
    // Even though a duals::dual<double> has 16 bytes, it is not necessarily aligned on a 16 bytes boundary...
  }
};

template<> struct conj_helper<Packet1dd, Packet1dd, false,true>
{
  EIGEN_STRONG_INLINE Packet1dd pmadd(const Packet1dd& x, const Packet1dd& y, const Packet1dd& c) const
  { return padd(pmul(x,y),c); }

  EIGEN_STRONG_INLINE Packet1dd pmul(const Packet1dd& a, const Packet1dd& b) const
  {
    return internal::pmul(a, pconj(b));
  }
};

template<> struct conj_helper<Packet1dd, Packet1dd, true,false>
{
  EIGEN_STRONG_INLINE Packet1dd pmadd(const Packet1dd& x, const Packet1dd& y, const Packet1dd& c) const
  { return padd(pmul(x,y),c); }

  EIGEN_STRONG_INLINE Packet1dd pmul(const Packet1dd& a, const Packet1dd& b) const
  {
    return internal::pmul(pconj(a), b);
  }
};

template<> struct conj_helper<Packet1dd, Packet1dd, true,true>
{
  EIGEN_STRONG_INLINE Packet1dd pmadd(const Packet1dd& x, const Packet1dd& y, const Packet1dd& c) const
  { return padd(pmul(x,y),c); }

  EIGEN_STRONG_INLINE Packet1dd pmul(const Packet1dd& a, const Packet1dd& b) const
  {
    return pconj(internal::pmul(a, b));
  }
};

EIGEN_MAKE_CONJ_HELPER_CPLX_REAL(Packet1dd,Packet2d)

template<> EIGEN_STRONG_INLINE Packet1dd pdiv<Packet1dd>(const Packet1dd& a, const Packet1dd& b)
{
  const uint64x2_t mask = {0xffffffffffffffff, 0};
  const uint64x2_t imask = {0, 0xffffffffffffffff};
  return Packet1dd(vdivq_f64
                   (vaddq_f64(vreinterpretq_f64_u64(vandq_u64(mask, vreinterpretq_u64_f64(a.v))),
                              vreinterpretq_f64_u64(vandq_u64(imask,
                                        vreinterpretq_u64_f64(vsubq_f64(vmulq_f64(a.v,
                                                            vdupq_lane_f64(vget_low_f64(b.v), 0)),
                                                  vmulq_f64(vdupq_lane_f64(vget_low_f64(a.v), 0),
                                                            b.v)))))),
                    vaddq_f64(vreinterpretq_f64_u64(vandq_u64(mask, vreinterpretq_u64_f64(b.v))),
                              vreinterpretq_f64_u64(vandq_u64(imask,
                                        vreinterpretq_u64_f64(vmulq_f64(vdupq_lane_f64(vget_low_f64(b.v), 0),
                                                                        vdupq_lane_f64(vget_low_f64(b.v), 0))))))));
}

EIGEN_STRONG_INLINE Packet1dd pcplxflip/*<Packet1dd>*/(const Packet1dd& x)
{
  return Packet1dd(preverse(Packet2d(x.v)));
}

EIGEN_STRONG_INLINE void ptranspose(PacketBlock<Packet1dd,2>& kernel)
{
  Packet2d tmp = vcombine_f64(vget_high_f64(kernel.packet[0].v), vget_high_f64(kernel.packet[1].v));
  kernel.packet[0].v = vcombine_f64(vget_low_f64(kernel.packet[0].v), vget_low_f64(kernel.packet[1].v));
  kernel.packet[1].v = tmp;
}
#endif // EIGEN_ARCH_ARM64

} // end namespace internal

} // end namespace Eigen

#endif // EIGEN_DUAL_NEON_H

SHELL=bash
TARGET=cppduals
CMAKE_OPTIONS=-DCPPDUALS_TESTING=ON -DCPPDUALS_BENCHMARK=ON -DCPPDUALS_EIGEN_LATEST=OFF
CMAKE=cmake
DEFAULT_TARGET=package
GCC?=gcc-14
GXX?=g++-14
CLANG?=clang
CLANGXX?=clang++
CMAKE_BUILD_PARALLEL_LEVEL?=1
export CMAKE_BUILD_PARALLEL_LEVEL

all:
	[ `uname` != Darwin ] || $(MAKE) clang clangr cc ccr osx osxr ios iosr
	[ `uname` != Linux ]  || $(MAKE) cc ccr
	[ ! -d ~/.android ] || $(MAKE) android
	[ ! -f ~/.emscripten ] || $(MAKE) em

help:
	@echo "Make targets for :" $(TARGET)
	@echo " clang   	    - build debug package using clang compiler"
	@echo " clangr       - build release package using clang compiler"
	@echo " test         - "
	@echo " test-clang   - "
	@echo " osx          - build debug package using XCode command-line"
	@echo " osxr         - build release package using XCode command-line"
	@echo " cc           - build debug package using cc/gcc compiler"
	@echo " ccr          - build release package using cc/gcc compiler"
	@echo " and          - build android using gradle in ./Android"
	@echo " em-sdl2      - build debug package using cc/gcc compiler"
	@echo " em           ^ alias"
	@echo " em-sdl2r     - build release package using cc/gcc compiler"
	@echo " emr          ^ alias"
	@echo " "
	@echo " distclean    - remove ALL build directories from the above targets"
#	@echo " tags         - "


Build-clang/Makefile Build-clang/compile_commands.json:
	CC=$(CLANG) CXX=$(CLANGXX) $(CMAKE) . -BBuild-clang \
		-G'Unix Makefiles' \
		$(CMAKE_OPTIONS) \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_EXPORT_COMPILE_COMMANDS=1

Build-clangr/Makefile:
	CC=$(CLANG) CXX=$(CLANGXX) $(CMAKE) . -BBuild-clangr \
		-G'Unix Makefiles' \
		$(CMAKE_OPTIONS) \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo
		-DCMAKE_EXPORT_COMPILE_COMMANDS=1

Build-cc/Makefile:
	CC=$(GCC) CXX=$(GXX) $(CMAKE) . -BBuild-cc \
		-G'Unix Makefiles' \
		$(CMAKE_OPTIONS) \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
		-DCPPDUALS_TESTING=ON -DCODE_COVERAGE=ON

Build-ccr/Makefile:
	CC=$(GCC) CXX=$(GXX) $(CMAKE) . -BBuild-ccr \
		-G'Unix Makefiles' \
		$(CMAKE_OPTIONS) \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_EXPORT_COMPILE_COMMANDS=1

compile_commands.json: Build-clang/compile_commands.json
	[ -L compile_commands.json ] || ln -s Build-clang/compile_commands.json ./

clang: Build-clang/Makefile compile_commands.json
	$(CMAKE) --build Build-clang  --target $(DEFAULT_TARGET)

clangr: Build-clangr/Makefile
	$(CMAKE) --build Build-clangr --target $(DEFAULT_TARGET)

cc: Build-cc/Makefile
	$(CMAKE) --build Build-cc  --target $(DEFAULT_TARGET)

ccr: Build-ccr/Makefile
	$(CMAKE) --build Build-ccr --target $(DEFAULT_TARGET)

test: ccr clangr
	CTEST_OUTPUT_ON_FAILURE=y $(CMAKE) --build Build-ccr --target test
	CTEST_OUTPUT_ON_FAILURE=y $(CMAKE) --build Build-clangr --target test

cov: cc
	CMAKE_BUILD_PARALLEL_LEVEL=8 $(CMAKE) --build Build-cc --target test
	$(CMAKE) --build Build-cc --target cov-html

Build-osx/cppduals.xcodeproj:
	$(CMAKE) . -BBuild-osx -GXcode $(CMAKE_OPTIONS)

osx: Build-osx/cppduals.xcodeproj
	$(CMAKE) --build Build-osx --config Debug --target $(DEFAULT_TARGET)

osxr: Build-osx/cppduals.xcodeproj
	$(CMAKE) --build Build-osx --config Release --target $(DEFAULT_TARGET)

Frameworks.iOS/SDL2.framework/SDL2:
	echo "Running ./scripts/Build_SDL2_framework.sh ios"
	./scripts/Build_SDL2_framework.sh ios

and:
	cd Android && ./gradlew build

Build-emsc/Makefile:
	source ${HOME}/local/emsdk/emsdk_env.sh ; \
	emcmake $(CMAKE) . -BBuild-emsc -DBUILD_SDL2=OFF -DBUILD_FREETYPE=OFF \
		$(CMAKE_OPTIONS)
#		-DCMAKE_TOOLCHAIN_FILE=$${EMSCRIPTEN}/cmake/Modules/Platform/Emscripten.cmake

Build-emscr/Makefile:
	source ${HOME}/local/emsdk/emsdk_env.sh ; \
	emcmake $(CMAKE) . -BBuild-emscr -DBUILD_SDL2=OFF \
		-DBUILD_FREETYPE=OFF $(CMAKE_OPTIONS) -DCMAKE_BUILD_TYPE=Release

em-sdl2: Build-emsc/Makefile
	cd Build-emsc && $(MAKE) cppduals

em-sdl2r: Build-emscr/Makefile
	cd Build-emscr && $(MAKE) cppduals

em-glfw: Build-emscgl/Makefile
	cd Build-emscgl && $(MAKE) cppduals

em: em-sdl2

emrun: em-sdl2
	source ${HOME}/local/emsdk/emsdk_env.sh && cd Build-emsc/ && emrun --serve_after_close cppduals.html

emr: em-sdl2r

distclean:
	rm -rf Build-* Android/app/.cxx/

tags:
	cd Build-ccr && $(MAKE) tags

//===-- test_dual.cpp - test duals/dual -------------------------*- C++ -*-===//
//
// Part of the cppduals project.
// https://gitlab.com/tesch1/cppduals
//
// See https://gitlab.com/tesch1/cppduals/blob/master/LICENSE.txt for
// license information.
//
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
// (c)2025 Michael Tesch. tesch1@gmail.com
//
#include <duals/dual>
#include <complex>
#include <iomanip>
#include "gtest/gtest.h"

using duals::dualf;
using duals::duald;
using duals::dualld;
using duals::hyperdualf;
using duals::hyperduald;
using duals::hyperdualld;
typedef std::complex<double> complexd;
typedef std::complex<float> complexf;
typedef std::complex<hyperduald> chduald;
typedef std::complex<duald> cduald;
typedef std::complex<dualf> cdualf;
using duals::is_dual;
using duals::is_complex;
using duals::dual_traits;
using namespace duals::literals;

#define _EXPECT_TRUE(...)  {typedef __VA_ARGS__ tru; EXPECT_TRUE(tru::value); static_assert(tru::value, "sa"); }
#define _EXPECT_FALSE(...) {typedef __VA_ARGS__ fal; EXPECT_FALSE(fal::value); static_assert(!fal::value, "sa"); }
#define EXPECT_DEQ(A,B) EXPECT_EQ((A).rpart(), (B).rpart()); EXPECT_EQ((A).dpart(), (B).dpart())
#define EXPECT_DNE(A,B) EXPECT_NE((A).rpart(), (B).rpart()); EXPECT_NE((A).dpart(), (B).dpart())

TEST(cdualf, random2)
{
  // cdualf
  cdualf d1 = duals::randos::random2<cdualf>();
  cdualf d2 = duals::randos::random2<cdualf>();
  EXPECT_NE(d1.real().rpart(), 0);
  EXPECT_NE(d1.real().dpart(), 0);
  EXPECT_NE(d1.imag().rpart(), 0);
  EXPECT_NE(d1.imag().dpart(), 0);

  EXPECT_NE(d2.real().rpart(), 0);
  EXPECT_NE(d2.real().dpart(), 0);
  EXPECT_NE(d2.imag().rpart(), 0);
  EXPECT_NE(d2.imag().dpart(), 0);

  EXPECT_NE(d1.real().rpart(), d2.real().rpart());
  EXPECT_NE(d1.real().dpart(), d2.real().dpart());

  EXPECT_NE(d1.imag().rpart(), d2.imag().rpart());
  EXPECT_NE(d1.imag().dpart(), d2.imag().dpart());
}

TEST(cduald, random2) {
  // cduald
  cduald d1 = duals::randos::random2<cduald>();
  cduald d2 = duals::randos::random2<cduald>();
  EXPECT_NE(d1.real().rpart(), 0);
  EXPECT_NE(d1.real().dpart(), 0);
  EXPECT_NE(d1.imag().rpart(), 0);
  EXPECT_NE(d1.imag().dpart(), 0);

  EXPECT_NE(d2.real().rpart(), 0);
  EXPECT_NE(d2.real().dpart(), 0);
  EXPECT_NE(d2.imag().rpart(), 0);
  EXPECT_NE(d2.imag().dpart(), 0);

  EXPECT_NE(d1.real().rpart(), d2.real().rpart());
  EXPECT_NE(d1.real().dpart(), d2.real().dpart());

  EXPECT_NE(d1.imag().rpart(), d2.imag().rpart());
  EXPECT_NE(d1.imag().dpart(), d2.imag().dpart());
}

#define TEST_ELEMWISE(OP, name, CDUAL_T) \
  TEST(cdual_##CDUAL_T, name) { \
    CDUAL_T d1 = duals::randos::random2<CDUAL_T>(); \
    CDUAL_T d2 = duals::randos::random2<CDUAL_T>(); \
    CDUAL_T r = d1 OP d2; \
    EXPECT_EQ(d1.real().rpart() OP d2.real().rpart(), r.real().rpart()); \
    EXPECT_EQ(d1.imag().rpart() OP d2.imag().rpart(), r.imag().rpart()); \
    EXPECT_EQ(d1.real().dpart() OP d2.real().dpart(), r.real().dpart()); \
    EXPECT_EQ(d1.imag().dpart() OP d2.imag().dpart(), r.imag().dpart()); \
  }

TEST_ELEMWISE(+,add,cdualf)
TEST_ELEMWISE(-,sub,cdualf)
TEST_ELEMWISE(+,add,cduald)
TEST_ELEMWISE(-,sub,cduald)

TEST(cduald, mul)
{
  // 1) Generate random inputs
  cduald d1 = duals::randos::random2<cduald>();
  cduald d2 = duals::randos::random2<cduald>();
  duald a = d1.real();
  duald b = d1.imag();
  duald c = d2.real();
  duald d = d2.imag();

  EXPECT_NE(a.rpart(), 0);
  EXPECT_NE(a.dpart(), 0);
  EXPECT_NE(b.rpart(), 0);
  EXPECT_NE(b.dpart(), 0);
  EXPECT_NE(c.rpart(), 0);
  EXPECT_NE(c.dpart(), 0);
  EXPECT_NE(d.rpart(), 0);
  EXPECT_NE(d.dpart(), 0);

  // 2) Do the division in your scalar cduald:
  cduald r = d1 * d2;
  
  // 3) Compute a reference result using double-based (or some known-correct) division:
  cduald ref;

  ref.real(a * c - b * d);
  ref.imag(a * d + b * c);
  
  // 4) Compare each sub-component with some tolerance
  //    (the same tolerance you used in your packet test, or something smaller)
  float tol = 1e-10f;  // or use e.g. 20*std::numeric_limits<float>::epsilon()
  
  // Compare real-part(rpart), real-part(dpart), imag-part(rpart), imag-part(dpart)
  EXPECT_NEAR(r.real().rpart(), ref.real().rpart(), tol);
  EXPECT_NEAR(r.real().dpart(), ref.real().dpart(), tol);
  EXPECT_NEAR(r.imag().rpart(), ref.imag().rpart(), tol);
  EXPECT_NEAR(r.imag().dpart(), ref.imag().dpart(), tol);
}

TEST(cduald, div)
{
  // 1) Generate random inputs
  cduald d1 = duals::randos::random2<cduald>();
  cduald d2 = duals::randos::random2<cduald>();
  duald a = d1.real();
  duald b = d1.imag();
  duald c = d2.real();
  duald d = d2.imag();

  EXPECT_NE(a.rpart(), 0);
  EXPECT_NE(a.dpart(), 0);
  EXPECT_NE(b.rpart(), 0);
  EXPECT_NE(b.dpart(), 0);
  EXPECT_NE(c.rpart(), 0);
  EXPECT_NE(c.dpart(), 0);
  EXPECT_NE(d.rpart(), 0);
  EXPECT_NE(d.dpart(), 0);

  // 2) Do the division in your scalar cduald:
  cduald r = d1 / d2;
  
  // 3) Compute a reference result using double-based (or some known-correct) division:
  cduald ref;

  ref.real((a * c + b * d) / (c * c + d * d));
  ref.imag((b * c - a * d) / (c * c + d * d));
  
  // 4) Compare each sub-component with some tolerance
  //    (the same tolerance you used in your packet test, or something smaller)
  float tol = 1e-5f;  // or use e.g. 20*std::numeric_limits<float>::epsilon()
  
  // Compare real-part(rpart), real-part(dpart), imag-part(rpart), imag-part(dpart)
  EXPECT_NEAR(r.real().rpart(), ref.real().rpart(), tol);
  EXPECT_NEAR(r.real().dpart(), ref.real().dpart(), tol);
  EXPECT_NEAR(r.imag().rpart(), ref.imag().rpart(), tol);
  EXPECT_NEAR(r.imag().dpart(), ref.imag().dpart(), tol);
}


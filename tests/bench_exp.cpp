//===-- bench_dual - test dual class ----------------------------*- C++ -*-===//
//
// Part of the cppduals project.
// https://gitlab.com/tesch1/cppduals
//
// See https://gitlab.com/tesch1/cppduals/blob/master/LICENSE.txt for
// license information.
//
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
// (c)2019 Michael Tesch. tesch1@gmail.com
//

#include <duals/dual_eigen>
#include <iostream>
#include <fstream>
#include <complex>
#include <memory>

#include "type_name.hpp"
#include <Eigen/Core>
#include <Eigen/Dense>

#include "eexpokit/padm.hpp"
#include "eexpokit/chbv.hpp"
#include "eexpokit/expv.hpp"
//#include "eexpokit/mexpv.hpp"
#include "benchmark/benchmark.h"

using namespace duals;

template< class T > struct type_identity { typedef T type; };

#if 0  // this might be interesting in comparing the complex vs. non-complex implementations of m.exp() directly.
namespace Eigen {
namespace internal {
template<typename T> struct is_exp_known_type;
template<typename T> struct is_exp_known_type<std::complex<T>> : is_exp_known_type<T> {};
}}
#endif

#include <unsupported/Eigen/MatrixFunctions>

/* encode the type into an integer for benchmark output */
template<typename Tp> struct type_num { /* should fail */ };
template<> struct type_num<float>     { static constexpr int id = 1; };
template<> struct type_num<double>     { static constexpr int id = 2; };
template<> struct type_num<long double> { static constexpr int id = 3; };
template<typename Tp> struct type_num<std::complex<Tp>> { static constexpr int id = 10 + type_num<Tp>::id; };
template<typename Tp> struct type_num<duals::dual<Tp>> { static constexpr int id = 100 + type_num<Tp>::id; };

using duals::dualf;
using duals::duald;
typedef std::complex<double> complexd;
typedef std::complex<float> complexf;
typedef std::complex<duald> cduald;
typedef std::complex<dualf> cdualf;
template <class T> using MatrixX = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;

#if 0
#define V_RANGE(V,NF) ->Arg(V*4/NF)->Arg(V*32/NF)->Arg(V*256/NF)->Arg(V*2048/NF)->Arg(V*1)->Complexity()
#else
#define V_RANGE(V,NF) ->Arg(V*1024/NF)
#endif


template <class Rt> void B_Expm(benchmark::State& state)
{
  int N = state.range(0);
  //Rt S(1);
  MatrixX<Rt> A = MatrixX<Rt>::Random(N, N);
  MatrixX<Rt> B = MatrixX<Rt>::Zero(N, N);
  //A = S * A / A.norm();

  for (auto _ : state) {
    B = A.exp();
    benchmark::ClobberMemory();
  }

  state.counters["type"] = type_num<Rt>::id;
  state.SetComplexityN(state.range(0));
}

template <class Rt> void B_ExpPadm(benchmark::State& state)
{
  int N = state.range(0);
  //Rt S(1);
  MatrixX<Rt> A = MatrixX<Rt>::Random(N, N);
  MatrixX<Rt> B = MatrixX<Rt>::Zero(N, N);
  //A = S * A / A.norm();

  for (auto _ : state) {
    B = eexpokit::padm(A);
    benchmark::ClobberMemory();
  }

  state.counters["type"] = type_num<Rt>::id;
  state.SetComplexityN(state.range(0));
}

template <class Rt> void B_ExpExpv(benchmark::State& state)
{
  int N = state.range(0);
  //Rt S(1);
  MatrixX<Rt> A = MatrixX<Rt>::Zero(N, N);
  MatrixX<Rt> b = MatrixX<Rt>::Ones(N, 1);
  MatrixX<Rt> c = MatrixX<Rt>::Zero(N, 1);
  //A = S * A / A.norm();

  // sparse random fill
  for (int i = 0; i < 4*N; i++)
    A((int)duals::randos::random(0.,N-1.),
      (int)duals::randos::random(0.,N-1.)) = duals::randos::random2<Rt>();

  for (auto _ : state) {
    auto ret = eexpokit::expv(1,A,b);
    if (ret.err > 1) {
      std::ofstream f("fail.m");
      f << "A=" << A.format(eexpokit::OctaveFmt) << "\n";
      break;
    }
    // c = ret.w
    benchmark::ClobberMemory();
  }

  state.counters["type"] = type_num<Rt>::id;
  state.SetComplexityN(state.range(0));
}

template <class Rt> void B_ExpChbv(benchmark::State& state)
{
  int N = state.range(0);
  //Rt S(1);
  MatrixX<Rt> A = MatrixX<Rt>::Random(N, N);
  MatrixX<Rt> b = MatrixX<Rt>::Zero(N, 1);
  MatrixX<Rt> c = MatrixX<Rt>::Zero(N, 1);
  //A = S * A / A.norm();

  for (auto _ : state) {
    c = eexpokit::chbv(A,b);
    benchmark::ClobberMemory();
  }

  state.counters["type"] = type_num<Rt>::id;
  state.SetComplexityN(state.range(0));
}

#define MAKE_BM_SIMPLE(TYPE1,NF)                                  \
  BENCHMARK_TEMPLATE(B_ExpPadm, TYPE1) V_RANGE(1,NF);             \
  BENCHMARK_TEMPLATE(B_ExpChbv, TYPE1) V_RANGE(1,NF);             \
  BENCHMARK_TEMPLATE(B_ExpExpv, TYPE1) V_RANGE(1,NF)

#define MAKE_BENCHMARKS(TYPE1,NF)                                 \
  MAKE_BM_SIMPLE(TYPE1,NF);                                       \
  BENCHMARK_TEMPLATE(B_Expm, TYPE1) V_RANGE(1,NF)

//  BENCHMARK_TEMPLATE(B_VecVecMulCXX, TYPE1,TYPE2) V_RANGE(4,NF);
//  BENCHMARK_TEMPLATE(B_MatMatCXX, TYPE1,TYPE2) V_RANGE(1,NF);

#if 1
MAKE_BENCHMARKS(float, 1);
MAKE_BENCHMARKS(complexf, 2);
MAKE_BM_SIMPLE(dualf, 2);
MAKE_BM_SIMPLE(cdualf, 4);
#else
MAKE_BENCHMARKS(double, 1);
MAKE_BENCHMARKS(complexd, 2);
MAKE_BENCHMARKS(duald, 2);
MAKE_BENCHMARKS(cduald, 4);
#endif

#define QUOTE(...) STRFY(__VA_ARGS__)
#define STRFY(...) #__VA_ARGS__

int main(int argc, char** argv)
{
#ifndef EIGEN_VECTORIZE
  //static_assert(false, "no vectorization?");
#endif
  std::cout << "OPT_FLAGS=" << QUOTE(OPT_FLAGS) << "\n";
  std::cout << "INSTRUCTIONSET=" << Eigen::SimdInstructionSetsInUse() << "\n";
  ::benchmark::Initialize(&argc, argv);
  ::benchmark::RunSpecifiedBenchmarks();
}
